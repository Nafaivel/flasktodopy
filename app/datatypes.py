from enum import Enum
import re
import json
from bson import ObjectId


class TodoState(Enum):
    Done = "X"
    Undone = ""


class Todo:
    state: TodoState
    content: str
    priority: str
    """should one of A-Z or ' ', '-', ''"""

    def __init__(self, content: str, priority="-"):
        """priority should one of A-Z or ' ', '-', ''"""
        # should be string or crash program without catching
        self.priority = Todo._check_priority(priority)
        self.content = content
        self.state = TodoState.Undone

    def change_content(self, content: str):
        self.content = content

    def change_priority(self, priority: str) -> None | ValueError:
        self.priority = Todo._check_priority(priority)

    def undone(self):
        self.state = TodoState.Undone

    def done(self):
        self.state = TodoState.Done

    @staticmethod
    def from_dict(json: dict):
        """
        json is request.json or request.get_json() from flask data example
        {
        "content": "task description",
        "priority": "A",
        "state": ""
        }
        """
        content = json.get("content")
        if content == None:
            content = ""
        todo = Todo(content)
        priority = json.get("priority")
        if priority != None:
            todo.change_priority(priority)
        state = json.get("state")
        if state != None:
            todo.state = TodoState(state)
        return todo

    @staticmethod
    def from_dict_to_bson_updated(json: dict) -> dict[str, str]:
        data_to_update = {}
        content = json.get("content")
        if content != None:
            data_to_update["content"] = content
        priority = json.get("priority")
        if priority != None:
            try:
                # throws error or returns value
                data_to_update["priority"] = Todo._check_priority(priority)
            except:
                pass
        state = json.get("state")
        if state != None:
            try:
                # creates enum or thows error
                state = TodoState(state)
                data_to_update["state"] = state.value
            except:
                pass

        print(data_to_update)

        return data_to_update

    def to_dict(self) -> dict:
        to_dict = self.__dict__
        to_dict["state"] = to_dict["state"].value
        return to_dict

    def to_bson(self) -> dict:
        """to dict in fact"""
        return self.to_dict()

    def to_json(self) -> str:
        return json.dumps(self.to_dict())

    @staticmethod
    def _check_priority(priority: str) -> str | ValueError:
        """validates the priority and returns it or ValueError"""
        if len(priority) == 0:
            pass
        elif len(priority) != 1:
            raise ValueError(
                "priority should be A-Z or '' or ' ' or '-' and with length less or equal 1"
            )
        elif not re.match(r"^[A-Za-z \-]", priority):
            raise ValueError(
                "priority should be A-Z or '' or ' ' or '-' and with length less or equal 1"
            )

        return priority.upper()


def test_check_priority_mutation():
    # here should be ValueError
    try:
        Todo._check_priority("|")
    except ValueError:
        pass
    except:
        raise


def test_check_priority():
    assert Todo._check_priority("A") == "A"
