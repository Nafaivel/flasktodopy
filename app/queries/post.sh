#!/bin/bash
curl \
  -X POST \
  -H "Content-type: application/json" \
  -d "{\"content\" : \"$1\"}" \
  127.0.0.1:8080/todos
