#!/bin/bash
curl \
  -X POST \
  -H "Content-type: application/json" \
  -d "{\"priority\" : \"$1\", \"content\" : \"$2\"}" \
  127.0.0.1:8080/todos
