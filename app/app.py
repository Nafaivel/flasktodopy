from flask import Flask, render_template, request
from datatypes import Todo
from pymongo import MongoClient
from bson import ObjectId
import os
import json

# TODO: handle if not set (None)?
mongo_host = os.getenv("MONGO_HOST") or "127.0.0.1:27017"
mongo_username = os.getenv("MONGO_USERNAME")
mongo_passwd = os.getenv("MONGO_PASSWORD")

client = MongoClient(
    serverSelectionTimeoutMS=3000,
    host=mongo_host,
    username=mongo_username,
    password=mongo_passwd,
)
db = client.flasktodopy
todo_collection = db["todos"]

app = Flask(__name__)


@app.get("/todos")
def get_todos():
    docs = list(todo_collection.find())
    response = "["
    for doc in docs:
        response += "\n  " + str(doc) + ","
    response += "\n]"
    return response


@app.post("/todos")
def add_todo():
    data = request.get_json()
    if data != None:
        todo: dict = Todo.from_dict(data).to_bson()
        result = todo_collection.insert_one(todo)
        return dict_to_json(todo, result.inserted_id), 201
    return "", 400


# NOTE: cause ObjectId and Enum don't serializable with json.dumps i write this... ehh where is serde or something like this in python
def dict_to_json(obj: dict, id: ObjectId|None = None) -> str:
    """yeah it's just replaces _id ObjectId(id) with id or ads it"""
    if id != None:
        obj["_id"] = str(id)
    else:
        obj["_id"] = str(obj["_id"])
    return json.dumps(obj)


def find_todo_by_uuid(todo_id) -> dict | None:
    return todo_collection.find_one({"_id": ObjectId(todo_id)})


@app.get("/todos/<todo_id>")
def get_todo_by_uuid(todo_id):
    doc = find_todo_by_uuid(todo_id)
    if doc != None:
        return dict_to_json(doc), 200
    else:
        return "", 404


@app.put("/todos/<todo_id>")
def update_todo_by_uuid(todo_id):
    data = request.get_json()
    if data != None:
        to_update_in_todo = Todo.from_dict_to_bson_updated(data)
        todo_collection.update_one(
            {"_id": ObjectId(todo_id)}, {"$set": to_update_in_todo}
        )
        return dict_to_json(to_update_in_todo), 201
    # just for fun anyway doesn't reachable
    return "", 418


if __name__ == "__main__":
    app.run(port=8080, host="0.0.0.0")
