# flsktodopy

## What it is
This is project used to develop/learn Flask todo app with mongodb rised with docker-compose

## Requirements
- Setted up docker-compose
- Curl (optionaly some of requests done with curl)

## How to run
clone project \
cd to project root \
`docker-compose up`

go to app/queries to check examples with curl
